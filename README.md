This is a sample C# project that builds are the GWS Part 1 sample and shows how to make requests.

The accompanying article can be found [here](http://docs.genesys.com/developer/index.php/2015/09/02/developing-for-gws-in-net-part-2-making-requests/) at the Genesys Developer Portal.