﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CometD.Bayeux;
using CometD.Bayeux.Client;
using CometD.Client;
using CometD.Client.Transport;
using RestSharp;
using System.Windows;
using Newtonsoft.Json.Linq;

namespace GWS.NET
{
    public class GWSClient
    {
        private BayeuxClient bayeuxClient;
        private RestClient restClient;
        private String gwsUrl;
        private String username;
        private String basicAuth;
        private String csrfHeader;
        private String csrfToken;
        private CookieContainer cookieContainer;

        public GWSClient(string gwsUrl, string username, string password)
        {
            this.gwsUrl = gwsUrl;
            this.username = username;

            Debug.WriteLine("Initized for GWS at " + this.gwsUrl + " as " + this.username);

            /**
             * GWS uses HTTP basic authentication to authenticate that requests are coming from a defined Genesys user.
             * To support that we need to base64 encode the string "<username>:<password>" and then request to have
             * that base64 string added to the 'Authorization' key in the HTTP header every time that the CometD
             * library performs a long polling request.
             */
            this.basicAuth = "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            cookieContainer = new CookieContainer();
            /**
             * Create an instance of a RestClient initialized to the GWSUrl 
             * to use when we want to make requests.
             */
            restClient = new RestClient(gwsUrl);
            restClient.CookieContainer = cookieContainer;
        }

        public void Connect()
        {
            Debug.WriteLine("Connecting to GWS CometD event stream");

            var options = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { HttpRequestHeader.Authorization.ToString(), this.basicAuth }
               ,{ csrfHeader, csrfToken }
            };

            /**
             * GWS currently only supports LongPolling as a method to receive events.
             * So tell the CometD library to negotiate a handshake with GWS and setup a LongPolling session.
             */
            bayeuxClient = new BayeuxClient(this.gwsUrl + "/api/v1/notifications", null, new LongPollingTransport(options));

            if ( bayeuxClient.Handshake(null, 30000) )
            {
                Debug.WriteLine("Handshake with GWS Successful");

                /**
                 * The CometD protocol supports the idea of subscribing to different channels, and in fact GWS
                 * publishes events on various channels.  However you can request to be notified of all messages
                 * on all channels by specifying a channel name of '/**', which is what we will do in this sample.
                 */
                IClientSessionChannel channel = bayeuxClient.GetChannel("/**");
                channel.Subscribe(new CallbackMessageListener<BayeuxClient>(OnMessageReceived, bayeuxClient));
            } 
            else
            {
                throw new Exception("Unable to establish CometD handshake with GWS");
            }
        }

        public void Disconnect()
        {
            Debug.WriteLine("Disconnecting from GWS CometD event stream");

            if ( bayeuxClient != null && bayeuxClient.IsConnected )
            {
                bayeuxClient.Disconnect();
            }
        }

        public event GWSEventHandler GWSEventReceived;
        public delegate void GWSEventHandler(IMessage message);

        public void OnMessageReceived(IClientSessionChannel channel, IMessage message, BayeuxClient client)
        {
            Debug.WriteLine("GWSClient received message on channel " + message.Channel + ": " + message.Data.ToString());

            if ( GWSEventReceived != null )
            {
                GWSEventReceived(message);
            }
        }

        public void SendRequest(GWSRequest gwsRequest, Action<IRestResponse> callback)
        {
            RestRequest restRequest = new RestRequest(gwsRequest.ResourceUri, gwsRequest.Method);

            /**
             * Include the required HTTP Basic Authorization header with the request
             */
            restRequest.AddHeader("Authorization", this.basicAuth);

            if ( csrfHeader != null && csrfToken != null )
            {
                restRequest.AddHeader(csrfHeader, csrfToken);
            }

            /**
             * Only POST and PUT methods should have content
             */
            if ( gwsRequest.Method == Method.POST ||
                 gwsRequest.Method == Method.PUT )
            {
                restRequest.RequestFormat = RestSharp.DataFormat.Json;
                JObject json = JObject.Parse(gwsRequest.Content);
                restRequest.AddParameter("application/json", json.ToString(), ParameterType.RequestBody);
            }

            /**
             * Since we are making a remote request that could potentially take some time, we certainly don't want to block
             * any calling threads, especially the UI thread.  When the response comes back we want to send it back to the 
             * callback and as a convenience we'll marshal it to the UI thread if that is necessary.
             */
            restClient.ExecuteAsync(restRequest, restResponse => {

                if (csrfHeader == null && csrfToken == null)
                {
                    foreach(var header in restResponse.Headers)
                    {
                        if (header.Name.Equals("X-CSRF-HEADER"))
                        {
                            csrfHeader = (string)header.Value;
                        }

                        if (header.Name.Equals("X-CSRF-TOKEN"))
                        {
                            csrfToken = (string)header.Value;
                        }
                    }
                }

                if ( !Application.Current.Dispatcher.CheckAccess() )
                {
                    Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        callback(restResponse);
                    }));
                }
            });
        }
    }
}
