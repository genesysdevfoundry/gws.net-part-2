﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GWS.NET
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GWSClient gwsClient;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                gwsClient = new GWSClient(Properties.Settings.Default.GWSUrl,
                                          Properties.Settings.Default.Username,
                                          Properties.Settings.Default.Password);
                gwsClient.GWSEventReceived += gwsClient_GWSEventReceived;
            }
            catch( Exception exc )
            {
                MessageBox.Show(exc.Message, "GWS CometD Error", MessageBoxButton.OK);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        void gwsClient_GWSEventReceived(CometD.Bayeux.IMessage message)
        {
            if ( !this.Dispatcher.CheckAccess() )
            {
                this.Dispatcher.BeginInvoke(new GWSClient.GWSEventHandler(gwsClient_GWSEventReceived), new object[] {message});
                return;
            }

            JObject json = JObject.Parse(message.ToString());

            this.tbxEvents.Text = json.ToString();

            string messageType = (string)json["data"]["messageType"];

            switch (messageType)
            {
                case "DeviceStateChangeMessage":
                    Debug.WriteLine("TODO: Process DeviceStateChangeMessage");
                    break;

                case "ChannelStateChangeMessageV2":
                    Debug.WriteLine("TODO: Process ChannelStateChangeMessageV2");
                    break;
            }
        }

        private void GetGWSVersion()
        {
            GWSRequest gwsRequest = new GWSRequest(Method.GET, "/api/v2/diagnostics/version");
            gwsClient.SendRequest(gwsRequest, restResponse =>
            {
                Debug.WriteLine("GetGWSVersion response: " + restResponse.Content);

                JObject json = JObject.Parse(restResponse.Content);

                this.tbxResponses.Text = json.ToString();

                if ((int)json["statusCode"] == 0)
                {
                    Debug.WriteLine("GWS Version = " + json["version"]);
                }
            });
        }

        private void GetMe()
        {
            GWSRequest gwsRequest = new GWSRequest(Method.GET, "/api/v2/me?subresources=*");
            gwsClient.SendRequest(gwsRequest, restResponse =>
            {
                Debug.WriteLine("GetMe response: " + restResponse.Content);

                JObject json = JObject.Parse(restResponse.Content);

                this.tbxResponses.Text = json.ToString();

                if ((int)json["statusCode"] == 0)
                {
                    //TODO: Update state of application with the 'Me' snapshot
                }
            });
        }

        private void StartContactCenterSession()
        {
            GWSRequest gwsRequest = new GWSRequest(Method.POST, "/api/v2/me", "{'operationName': 'StartContactCenterSession', 'channels': ['voice']}");

            gwsClient.SendRequest(gwsRequest, restResponse =>
            {
                Debug.WriteLine("StartContactCenterSession response: " + restResponse.Content);

                JObject json = JObject.Parse(restResponse.Content);

                this.tbxResponses.Text = json.ToString();

                if ((int)json["statusCode"] == 0)
                {
                    gwsClient.Connect(); 
                }
            });
        }
        
        private void EndContactCenterSession()
        {
            GWSRequest gwsRequest = new GWSRequest(Method.POST, "/api/v2/me", "{'operationName': 'EndContactCenterSession'}");

            gwsClient.SendRequest(gwsRequest, restResponse =>
            {
                Debug.WriteLine("EndContactCenterSession response: " + restResponse.Content);

                JObject json = JObject.Parse(restResponse.Content);

                this.tbxResponses.Text = json.ToString();

                if ((int)json["statusCode"] == 0)
                {
                    //gwsClient.Disconnect();
                }
            });
        }

        private void btnVersion_Click(object sender, RoutedEventArgs e)
        {
            this.GetGWSVersion();
        }

        private void btnMe_Click(object sender, RoutedEventArgs e)
        {
            this.GetMe();
        }

        private void btnStartContactCenterSession_Click(object sender, RoutedEventArgs e)
        {
            this.StartContactCenterSession();
        }

        private void btnEndContactCenterSession_Click(object sender, RoutedEventArgs e)
        {
            this.EndContactCenterSession();
        }
    }
}
