﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace GWS.NET
{
    public class GWSRequest
    {
        public Method Method { get; set; }
        public String ResourceUri { get; set; }
        public String Content { get; set; }

        public GWSRequest()
        {
        }

        public GWSRequest(Method method, string resourceUri, string content)
        {
            this.Method = method;
            this.ResourceUri = resourceUri;
            this.Content = content;
        }

        public GWSRequest(Method method, string resourceUri) : this(method, resourceUri, null)
        {
        }
    }
}
